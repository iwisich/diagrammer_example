
library(magrittr)
library(DiagrammeR)


node_names <- paste('C0', 1:9, sep = '')
costs <- rnorm(n = 50, mean = 100, sd = 10)


create_nodes(
  nodes = 1:10,
  label = TRUE,
  type = 'character',
  style = 'filled',
  color = 'lightblue',
  shape = 'circle',
  tooltip = node_names,
  data = costs
) -> nodes

create_edges(
  from = c(1, 2, 3, 3, 4, 5, 10, 10, 10),
  to = c(2, 3, 3, 4, 5, 6, 4, 5, 6),
  rel = 'leading_to',
  color = 'aqua',
  arrowsize = 0.5
) -> edges

create_graph(
  graph_name = 'first',
  nodes_df = nodes,
  edges_df = edges,
  graph_attrs = c('layout = dot',
                  'rankdir = LR'),
  node_attrs = 'fontname = Helvetica'
) -> G1

render_graph(G1)
